FROM ubuntu:18.04

#Python 3.7 install
RUN apt-get update && apt-get install python3.7 -y
RUN apt-get install python3-pip -y
RUN apt-get install ninja-build -y
RUN rm /usr/bin/python3
RUN ln -s python3.7 /usr/bin/python3
RUN apt-get install python-dev -y
RUN apt-get install python3-h5py -y
RUN apt-get install python3.7-dev -y
RUN pip3 install --upgrade pip
RUN pip3 install setuptools -U

#Various packages install
RUN DEBIAN_FRONTEND=noninteractive apt-get install wget -y
RUN apt-get -y install libssl-dev
RUN apt-get -y install libjpeg-dev libtiff5-dev
RUN apt-get -y install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
RUN apt-get -y install libxvidcore-dev libx264-dev
RUN apt-get -y install qt4-dev-tools libatlas-base-dev
RUN apt-get -y install cmake gfortran libhdf5-dev
RUN apt-get -y install libc-ares-dev libeigen3-dev gcc libgfortran5 libatlas3-base libopenblas-dev libopenblas-base libblas-dev liblapack-dev cython openmpi-bin libopenmpi-dev


#Install TensorFlow Lite
RUN wget https://github.com/lhelontra/tensorflow-on-arm/releases/download/v2.3.0/tensorflow-2.3.0-cp37-none-linux_armv7l.whl
RUN pip3 install tensorflow-2.3.0-cp37-none-linux_armv7l.whl
RUN rm -rf tensorflow-2.3.0-cp37-none-linux_armv7l.whl

#Install Python libraries
ADD ./requirements.txt /tmp
RUN pip3 install -r /tmp/requirements.txt
RUN pip3 install opencv-python==4.4.0.42
